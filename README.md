# GeryGoldPlotAssistant

#### 介绍

面向 Chia 的全自动智能 P 盘软件，软件初衷是提升散户的竞争力，所以该版本中不会出现代 P 和群控的相关功能  
软件自动根据用户设置的几个关键参数，再通过后台算法自动开启 Plot 任务，全程无需人工干预；只需要等待硬盘装满即可。  
软件不包含任何网络模块，同时也提醒使用带有网络模块的 P 图软件的用户，注意资金风险。  
个人版 P 盘助理会自动使用官方钱包当前登录的配置进行 P 盘，无需手动指定 FPK 和 PPK，同时个人版也不开放 FPK 和 PPK 的功能。

#### QQ群 632451653

#### 软件特性  
自动根据系统资源添加P盘任务  
自动检测多缓存盘负载、自动均衡任务  
自动进行任务错峰（参数设置合理的情况下）
自动搬运PLOT文件，写入时自动压力均衡、容量均衡  
最终目录盘符支持热插拔、随时调整  
设置参数支持随时调整、随时生效  
支持任务手动终结，自动删除相应的垃圾文件  

> 简单来说就是调好参数、自动错峰、自动启动任务、自动搬图


#### 安装教程

先[在这里下载](https://gitee.com/frontopen/gery-gold-plot-assistant/releases)安装包，解压后直接运行GeryGoldPlotAssistant.exe即可。  
软件使用.Net5框架开发，首次使用软件要加载运行环境，可能会稍慢。

#### 最佳使用实践  

1.打开软件，按照主要设置说明中的类目进行相关设置  
2.点击一次 添加任务按钮 验证参数是否设置正确，且可以立即开始第一个P图任务。  
3.躺尸等收菜

#### 不推荐的操作方式
1.CPU阈值直接拉满，这样的话软件无法实现错峰效果  
2.并发数大额超出硬件实际可运极限  
3.开始时直接手动把任务加满

#### 常见问题  
Q：我已经有chia.exe进程在跑了，我还可以开这个软件吗？  
A：可以，软件是一个助理级程序，只是暂时看不到外部的P盘任务，但是资源达到对应数量时还是可以自动添加任务。不过需要注意的是，由于程序在外部执行，并发限制可能会失效，建议不要提供太高的CPU阈值。待外部任务完成后可以再提高CPU阈值限制。

Q：我不小心把软件窗口关闭了怎么办？  
A：实话说，能把窗口“不小心关闭”还是有点难度的。不过不用担心，再开窗口即可，设置好参数等待程序后续自动追加任务。当然了，这里需要注意的与上一条问题类似。

Q：P图的公钥是怎么设置的？  
A：软件工作原理类似官方的钱包，P图是直接读取官方钱包中已经登录的公钥配置信息直接启动任务。所以在P图之前请确认你登录的是你想要使用的那个公钥账号。  

Q：如何查看我当前使用的是哪个公钥？  
A：win+R 输入cmd 打开命令行窗口，然后输入 %USERPROFILE%\AppData\Local\chia-blockchain\app-1.1.6\resources\app.asar.unpacked\daemon\chia.exe keys show  

Q：如何检查文件是否有效？  
A：win+R 输入cmd 打开命令行窗口，然后输入 %USERPROFILE%\AppData\Local\chia-blockchain\app-1.1.6\resources\app.asar.unpacked\daemon\chia.exe plots check  


#### 主要设置说明


> **CPU阈值**  
这项数值是与CPU平滑率相对应的，主要工作原理是通过与CPU平滑率进行比对，判断是否发起新任务。该值设置越低，错峰开启任务的效果越好，不过也不要设置过低造成任务无法启动。**建议先保持50%**观察一阵子，根据实际运行情况来适当加减。    

> **内存限制**  
该限制是指允许程序分配任务时考虑的最大内存用量，如果当前内存可用量小于单任务内存量，则新任务不会启动。建议**无脑拉满**。

> **开垦数量**  
这个是指你总共想产出多少个plot文件，实际上就是**无脑拉满**的。

> **缓存目录**  
这里选择你需要用来作为缓存的磁盘，该选项可以多选。设置好之后软件会自动寻找可用空间最大、当前任务压力最小的磁盘发起新任务。

> **最大并发数**  
这个是限制软件启动P图进程的最大并发数量，建议根据自己机器自行尝试参数，以达到利益最大化。

> **最终目录**  
这里是选择你用来存储plot文件的磁盘。该选项可以多选，自动找空闲最大的磁盘；在多任务并行装盘时会自动进行负载均衡。  


#### 其他功能说明
> 禁用绘图算法、排除最终目录、单任务设置 与官方保持一致，不做过多解释
  
> **按钮区域**  
添加任务 -> 可以手动插入一条新任务  
清除所有通知 -> 任务完成时，软件会发出浮动通知，持续时间为24小时。该按钮可以批量清除。  
清空文件移动进程池 -> 软件会自动搬运P好的plot绘图文件，有时候因为一些未知的原因造成搬运失败，这时候可能该图不会再次搬运，点击这个按钮可以让软件重新搬运该文件。

> **结束该进程**  
在P图日志界面中有一个结束该进程按钮。该功能可以结束当前的chia.exe进程，并且自动清理该进程产生的临时文件。操作时请仔细根据提示框内容进行操作。